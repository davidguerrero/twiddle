module	trigonometric_adder
	#(parameter	input_precission=0,
			Amaximun_sin_index=0,
			Amaximun_cos_index=0,
			Bmaximun_sin_index=0,
			Bmaximun_cos_index=0,
			output_precission=0,
			output_maximun_sin_index=0,			
			output_maximun_cos_index=0
			)
	(	input	[Amaximun_sin_index:-input_precission] sinA,
		input	[Amaximun_cos_index:-input_precission] cosA,
		input	[Bmaximun_sin_index:-input_precission] sinB,
		input	[Bmaximun_cos_index:-input_precission] cosB,
	 	output	[output_maximun_sin_index:-output_precission] sin,
		output	[output_maximun_cos_index:-output_precission] cos
	);
	wire	[Amaximun_sin_index:-2*input_precission] sinAcosB;
	wire	[Bmaximun_sin_index:-2*input_precission] cosAsinB;
	wire	[output_maximun_sin_index:-2*input_precission] sinAcosB_PLUS_cosAsinB;

	wire	[output_maximun_cos_index:-2*input_precission] cosAcosB;
	wire	[output_maximun_cos_index:-2*input_precission] sinAsinB;
	wire	[output_maximun_cos_index:-2*input_precission] cosAcosB_MINUS_sinAsinB;
	
	wire [-1:-input_precission] expanded_cosA,expanded_cosB;
	assign expanded_cosA={{(-Amaximun_cos_index-1){1'b1}},cosA};		
	assign expanded_cosB={{(-Bmaximun_cos_index-1){1'b1}},cosB};		
	assign sinAcosB=sinA*expanded_cosB;
	assign cosAsinB=expanded_cosA*sinB;
	assign cosAcosB=expanded_cosA*expanded_cosB;
	assign sinAsinB=sinA*sinB;

	assign sinAcosB_PLUS_cosAsinB=sinAcosB+cosAsinB;
	assign cosAcosB_MINUS_sinAsinB=cosAcosB-sinAsinB;

	assign sin=sinAcosB_PLUS_cosAsinB[output_maximun_sin_index:-output_precission]+sinAcosB_PLUS_cosAsinB[-output_precission-1];
	assign cos=cosAcosB_MINUS_sinAsinB[output_maximun_cos_index:-output_precission]+(cosAcosB_MINUS_sinAsinB[-output_precission-1]&(~&cosAcosB_MINUS_sinAsinB[output_maximun_cos_index:-output_precission]));

endmodule

