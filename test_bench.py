import mpmath
guard_bits=80

def generate_rom_test(samples_exponent,fractional_bits):
    if samples_exponent<4:
        raise root_generation('There must be at least 16 samples')
    if fractional_bits<1:
        raise root_generation('The precision can not be lower than 1')
  
    f = open(sys.argv[3]+'/test_ROM.v', 'w')
    rom_width=(1+fractional_bits)<<1
    f.write("module test_ROM(output reg ["+str(rom_width-1)+":0] dout, input ["+str(samples_exponent-1)+":0] addr);\n\n")
    f.write("always @(addr) begin\n")
    f.write("    case (addr)\n")
    
    maximun_value=(1<<fractional_bits)-1
    last_address=(1<<samples_exponent)-1
    data_width=str(fractional_bits)
    for address in xrange(last_address+1):
        with mpmath.workprec(fractional_bits+guard_bits):
            root=mpmath.expjpi(-mpmath.ldexp(address,1-samples_exponent))
        sin=int(mpmath.nint(mpmath.ldexp(root.imag,fractional_bits),prec=0))
        cosin=int(mpmath.nint(mpmath.ldexp(root.real,fractional_bits),prec=0))
        if sin<0:
            sin_sign='1'
            sin=-sin
        else:
            sin_sign='0'
        sin=min(sin,maximun_value)
        if cosin<0:
            cosin_sign='1'
            cosin=-cosin
        else:
            cosin_sign='0'
        cosin=min(cosin,maximun_value)
        f.write("        ")
        if address<last_address:
            f.write(str(address))
        else:
            f.write('default')
        f.write(": dout={1'b"+sin_sign+","+data_width+"'h"+format(sin,'x')+",1'b"+cosin_sign+","+data_width+"'h"+format(cosin,'x')+"};\n")
    f.write("    endcase\n")
    f.write("end\n\n")
    f.write("endmodule")
    f.close()

import sys
from shutil import copyfile
try:
    copyfile("twiddle_test.v",sys.argv[3]+"/twiddle_test.v")
    generate_rom_test(int(sys.argv[1]),int(sys.argv[2]))
except IndexError:
    print "arguments: <samples exponent> <fractional bits> <target directory>\n"
