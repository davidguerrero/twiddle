`include "trigonometric_calculator.v"

module twiddle_calculator
	(	input [`samples_exponent-1:0] d,
		output imaginary_sign,
		output [-1:-(`output_width)] imaginary_magnitude,
		output real_sign,
		output [-1:-(`output_width)] real_magnitude
	);
	wire [`output_width*2-1:0] data;
	wire [-1:-`output_width] sin, cosin,rom_sin,rom_cosin,inverse_sqrt_2;
	wire [`samples_exponent-4:0] addr;
	assign inverse_sqrt_2=`inverse_sqrt_2;
	assign addr= (d[`samples_exponent-4:0]	^	{(`samples_exponent-3){d[`samples_exponent-3]}})	+	d[`samples_exponent-3];
	trigonometric_calculator virtual_rom(addr,rom_sin,rom_cosin);
	assign {sin,cosin}= d[`samples_exponent-3] & ~|d[`samples_exponent-4:0]?
				{inverse_sqrt_2,inverse_sqrt_2} :
				{rom_sin,rom_cosin};
	assign real_sign=d[`samples_exponent-1]^d[`samples_exponent-2];
	assign imaginary_sign=~d[`samples_exponent-1];
	assign {imaginary_magnitude,real_magnitude}= d[`samples_exponent-2]^d[`samples_exponent-3] ? {cosin,sin} : {sin,cosin};
endmodule

