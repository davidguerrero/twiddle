This software generates synthesizable Verilog descriptions of twiddle factor calculators and the corresponding testbenches. The generators have a tree structure. The number of samples of the DFT must be a power of 2.

To generate the description execute


>python twiddle.py samples_exponent fractional_bits number_of_stages

The first argument is the logarithm to base 2 of the number of samples. The second argument is the number of fractional bits used to represent the twiddle factors. The last argument is the height of the tree structure of the generator. The description will be stored in a folder with the name  `twiddle_calculator_<argument1>_<argument2>_<argument3>`.
After a description has been generated it is also possible to generate the corresponding testbench by executing

>python test_bench.py samples_exponent fractional_bits folder_name

The first argument is the logarithm to base 2 of the number of samples of the generator. The second argument is the number of fractional bits that the generator uses to represent the twiddle factors. The last argument is the name of the folder containing the description of the generator.